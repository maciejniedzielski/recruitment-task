import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { Location } from "@angular/common";

import { NewsItemComponent } from "./news-item.component";
import { RouterTestingModule } from "@angular/router/testing";
import { ImagePlaceholderPipe } from "../shared/pipes/image-placeholder.pipe";
import { Article } from "angular-news-api";
import { NewsDetailComponent } from "../news-detail/news-detail.component";
import { MockPipe, MockComponent } from "ng-mocks";
import { UrlOriginPipe } from "../shared/pipes/url-origin.pipe";

const mockArticle = {
    source: {
        id: "buzzfeed",
        name: "Buzzfeed",
    },
    author:
        '[{"@type":"Person","name":"Vylet Bertoncino","url":"https://www.buzzfeed.com/slalsaturtle","jobTitle":"Community Contributor"}]',
    title:
        "Decorate A Gingerbread House And We'll Reveal Which North Pole Resident You Are",
    description: "Gumdrops? Chocolate? What about coconut snow?",
    url:
        "https://www.buzzfeed.com/slalsaturtle/decorate-a-gingerbread-house-and-we-will-tell-you-1l4zdtq6d3",
    urlToImage:
        "https://img.buzzfeed.com/buzzfeed-static/static/2020-11/18/20/enhanced/b537877860a1/original-13696-1605730856-23.jpg?crop=3809:2000;0,0%26downsize=1250:*",
    publishedAt: "2020-11-23T18:52:22.0637279Z",
    content:
        "Sign up to the BuzzFeed Quizzes Newsletter - Binge on the latest quizzes delivered right to your inbox with the Quizzes newsletter!",
} as Article;

describe("NewsItemComponent", () => {
    let component: NewsItemComponent;
    let fixture: ComponentFixture<NewsItemComponent>;
    let location: Location;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule.withRoutes([
                    {
                        path: "article",
                        component: MockComponent(NewsDetailComponent),
                    },
                ]),
            ],
            declarations: [
                NewsItemComponent,
                MockPipe(ImagePlaceholderPipe),
                MockComponent(NewsDetailComponent),
                MockPipe(UrlOriginPipe),
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NewsItemComponent);
        component = fixture.componentInstance;
        component.article = mockArticle;
        fixture.detectChanges();
        location = TestBed.get(Location);
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should redirect to detail page on image click", () => {
        let element = fixture.debugElement.nativeElement.querySelector(
            "#image"
        );
        element.click();

        fixture.whenStable().then(() => {
            expect(location.path()).toBe("/article");
        });
    });

    it("should redirect to detail page on title click", () => {
        let element = fixture.debugElement.nativeElement.querySelector(
            "#title"
        );

        element.click();

        fixture.whenStable().then(() => {
            expect(location.path()).toBe("/article");
        });
    });
});
