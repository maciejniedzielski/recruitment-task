import { ChangeDetectionStrategy, Component, Input } from "@angular/core";
import { Article } from "angular-news-api";

@Component({
    selector: "app-news-item",
    templateUrl: "./news-item.component.html",
    styleUrls: ["./news-item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewsItemComponent {
    @Input() article: Article;
}
