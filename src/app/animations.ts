import {
    trigger,
    group,
    transition,
    animate,
    style,
    query,
} from "@angular/animations";

const slide = (direction: "left" | "right") => {
    return [
        query(
            ":enter, :leave",
            [
                style({
                    position: "absolute",
                    top: 0,
                    [direction]: 0,
                    width: "100%",
                }),
            ],
            { optional: true }
        ),
        query(":enter", [style({ [direction]: "-100%" })]),
        group([
            query(
                ":leave",
                [animate("100ms ease-out", style({ [direction]: "100%" }))],
                { optional: true }
            ),
            query(":enter", [
                animate("300ms ease-out", style({ [direction]: "0%" })),
            ]),
        ]),
    ];
};

export const slideInAnimation = trigger("routeAnimations", [
    transition("NewsComponent => NewsDetailComponent", slide("right")),
    transition("NewsDetailComponent => NewsComponent", slide("left")),
]);
