import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { NewsComment } from "../news.interfaces";

@Component({
    selector: "app-comment",
    templateUrl: "./comment.component.html",
    styleUrls: ["./comment.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CommentComponent {
    @Input() comment: NewsComment;
}
