import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { MockComponent, MockPipe } from "ng-mocks";
import { CommentComponent } from "../comment/comment.component";
import { AuthorNamePipe } from "../shared/pipes/author-name.pipe";
import { ImagePlaceholderPipe } from "../shared/pipes/image-placeholder.pipe";
import { UrlOriginPipe } from "../shared/pipes/url-origin.pipe";

import { NewsDetailComponent } from "./news-detail.component";

describe("NewsDetailComponent", () => {
    let component: NewsDetailComponent;
    let fixture: ComponentFixture<NewsDetailComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [
                NewsDetailComponent,
                MockPipe(ImagePlaceholderPipe),
                MockPipe(AuthorNamePipe),
                MockComponent(CommentComponent),
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NewsDetailComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
