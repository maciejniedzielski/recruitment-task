import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { Article } from "angular-news-api";
import { EMPTY, of, Subject } from "rxjs";
import {
    catchError,
    debounceTime,
    distinctUntilChanged,
    finalize,
    take,
    takeUntil,
} from "rxjs/operators";
import { NewsService } from "../news.service";

@Component({
    selector: "app-news",
    templateUrl: "./news.component.html",
    styleUrls: ["./news.component.scss"],
})
export class NewsComponent implements OnInit, OnDestroy {
    public articles: Article[] = [];
    public search: string;
    public loading: boolean = false;
    public error: boolean = false;
    public searchCtrl: FormControl = new FormControl();

    private subGuard$: Subject<void> = new Subject();

    constructor(private newsService: NewsService) {}

    public ngOnInit(): void {
        this.fetchArticles();
        this.listenOnSearchChange();
    }

    public ngOnDestroy(): void {
        this.subGuard$.next();
        this.subGuard$.complete();
    }

    private fetchArticles(search?: string): void {
        this.loading = true;
        this.newsService
            .topHeadlines(search)
            .pipe(
                finalize(() => (this.loading = false)),
                take(1),
                catchError(() => {
                    this.error = true;

                    return EMPTY;
                })
            )
            .subscribe((articles) => {
                this.articles = articles;
            });
    }

    private listenOnSearchChange(): void {
        this.searchCtrl.valueChanges
            .pipe(
                distinctUntilChanged(),
                debounceTime(500),
                takeUntil(this.subGuard$)
            )
            .subscribe((search) => {
                this.fetchArticles(search);
            });
    }
}
