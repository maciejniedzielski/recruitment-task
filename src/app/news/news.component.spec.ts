import { Injectable } from "@angular/core";
import {
    async,
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
} from "@angular/core/testing";
import { ReactiveFormsModule } from "@angular/forms";
import { MatInputModule, MatProgressBar } from "@angular/material";
import { MockComponent, MockModule } from "ng-mocks";
import { of } from "rxjs";
import { NewsItemComponent } from "../news-item/news-item.component";
import { NewsService } from "../news.service";

import { NewsComponent } from "./news.component";

@Injectable()
class NewsServiceStub {
    topHeadlines = jasmine.createSpy("topHeadlines").and.returnValue(of([]));
}

describe("NewsComponent", () => {
    let component: NewsComponent;
    let fixture: ComponentFixture<NewsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ReactiveFormsModule, MockModule(MatInputModule)],
            declarations: [
                NewsComponent,
                MockComponent(NewsItemComponent),
                MockComponent(MatProgressBar),
            ],
            providers: [{ provide: NewsService, useClass: NewsServiceStub }],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NewsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should fetch new articles on search form control change", fakeAsync(() => {
        const fetchArticlesSpy = spyOn<any>(component, "fetchArticles");
        const serviceSpy = fixture.debugElement.injector.get(
            NewsService
        ) as any;

        component.searchCtrl.patchValue("test");
        tick(500);
        fixture.detectChanges();

        expect(fetchArticlesSpy).toHaveBeenCalledTimes(1);
        expect(serviceSpy.topHeadlines).toHaveBeenCalledTimes(1);
    }));
});
