import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";

@Injectable()
export class NewsDetailGuard implements CanActivate {
    constructor(private router: Router) {}

    canActivate(next): boolean {
        /**
         * Make sure that this page can only be
         * accessible from the main News list with
         * a loaded article from 'state'
         */
        if (this.router.url !== "/news") {
            this.router.navigate(["/news"]);

            return false;
        }

        return true;
    }
}
