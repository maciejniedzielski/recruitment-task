import { Injectable } from "@angular/core";
import { Article, NewsApiService, TopHeadlinesConfig } from "angular-news-api";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
    providedIn: "root",
})
export class NewsService {
    constructor(private newsApiService: NewsApiService) {}

    /**
     * Fetch top headlines.
     * Pass query as argument to search articles by search query.
     */
    topHeadlines(query?: string): Observable<Article[]> {
        const config: TopHeadlinesConfig = {
            language: "en",
        };

        if (query) {
            config.q = query;
        }

        return this.newsApiService
            .topHeadlines(config)
            .pipe(map((res) => res.articles));
    }
}
