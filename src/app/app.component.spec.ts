import { TestBed, async } from "@angular/core/testing";
import {
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatToolbarModule,
} from "@angular/material";
import { RouterTestingModule } from "@angular/router/testing";
import { AppComponent } from "./app.component";

describe("AppComponent", () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                MatButtonModule,
                MatMenuModule,
                MatIconModule,
                MatToolbarModule,
            ],
            declarations: [AppComponent],
        }).compileComponents();
    }));

    it("should create the app", () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    it(`should have as title 'News'`, () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual("News");
    });

    // it("should render 'News' in a header title", () => {
    //     const fixture = TestBed.createComponent(AppComponent);
    //     fixture.detectChanges();
    //     const compiled = fixture.debugElement.nativeElement;

    //     expect(compiled.querySelector("#headerTitle").textContent).toEqual(
    //         "News"
    //     );
    // });
});
