import { TestBed } from "@angular/core/testing";
import { NewsApiService } from "angular-news-api";
import { of } from "rxjs";

import { NewsService } from "./news.service";

describe("NewsService", () => {
    let service: NewsService;
    let newsApiServiceSpy: jasmine.SpyObj<NewsApiService>;

    beforeEach(() => {
        const spy = jasmine.createSpyObj("NewsApiService", ["topHeadlines"]);

        TestBed.configureTestingModule({
            providers: [{ provide: NewsApiService, useValue: spy }],
        });

        service = TestBed.get(NewsService);
        newsApiServiceSpy = TestBed.get(
            NewsApiService
        ) as jasmine.SpyObj<NewsApiService>;
        newsApiServiceSpy.topHeadlines.and.returnValue(of([]));
    });

    it("should create NewsService", () => {
        expect(service).toBeDefined();
    });

    it("should get articles from data service", () => {
        service.topHeadlines();

        expect(newsApiServiceSpy.topHeadlines).toHaveBeenCalledTimes(1);
        expect(newsApiServiceSpy.topHeadlines).toHaveBeenCalledWith({
            language: "en",
        });
    });

    it("should get articles by search value from data service", () => {
        service.topHeadlines("test");

        expect(newsApiServiceSpy.topHeadlines).toHaveBeenCalledTimes(1);
        expect(newsApiServiceSpy.topHeadlines).toHaveBeenCalledWith({
            language: "en",
            q: "test",
        });
    });
});

class NewsApiServiceStub {
    topHeadlines = jasmine.createSpy("topHeadlines").and.returnValue(of([]));
}
