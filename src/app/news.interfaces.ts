export type NewsComment = {
    name: string;
    date: Date;
    avatar: string;
    comment: string;
};
