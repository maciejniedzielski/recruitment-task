import { NgModule } from "@angular/core";
import { AuthorNamePipe } from "./pipes/author-name.pipe";
import { ImagePlaceholderPipe } from "./pipes/image-placeholder.pipe";
import { UrlOriginPipe } from "./pipes/url-origin.pipe";

const PIPES = [UrlOriginPipe, ImagePlaceholderPipe, AuthorNamePipe];

@NgModule({
    declarations: [PIPES],
    exports: [PIPES],
})
export class SharedModule {}
