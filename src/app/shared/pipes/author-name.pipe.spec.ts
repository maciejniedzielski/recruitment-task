import { AuthorNamePipe } from "./author-name.pipe";

describe("AuthorNamePipe", () => {
    let pipe: AuthorNamePipe;

    beforeEach(() => {
        pipe = new AuthorNamePipe();
    });

    it("should create an instance", () => {
        expect(pipe).toBeTruthy();
    });

    it("should return author if author is not JSON", () => {
        const result = pipe.transform("John Doe");
        expect(result).toBe("John Doe");
    });

    it("should return empty string if author is JSON without array", () => {
        const result = pipe.transform('{"name":"John Doe"}');
        expect(result).toBe("");
    });

    it("should return person name if author is JSON with single item in array", () => {
        const result = pipe.transform('[{"name":"John Doe"}]');
        expect(result).toBe("John Doe");
    });

    it("should return persons names if author is JSON with multiple item in array", () => {
        const result = pipe.transform(
            '[{"name":"John Doe 1"}, {"name":"John Doe 2"}]'
        );
        expect(result).toBe("John Doe 1, John Doe 2");
    });
});
