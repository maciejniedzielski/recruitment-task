import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "authorName",
})
export class AuthorNamePipe implements PipeTransform {
    transform(author: string): string {
        try {
            const parsedAuthor = JSON.parse(author);

            /**
             * Sometimes the API returns article author as JSON object
             * that contains persons array, so we want to extract theirs names.
             */
            if (parsedAuthor.length) {
                return parsedAuthor.map((person) => person.name).join(", ");
            }

            return "";
        } catch (error) {
            return author;
        }
    }
}
