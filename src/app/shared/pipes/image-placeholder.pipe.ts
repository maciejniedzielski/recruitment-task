import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "imagePlaceholder",
})
export class ImagePlaceholderPipe implements PipeTransform {
    transform(url: string): string {
        return url ? url : "assets/images/placeholder.jpg";
    }
}
