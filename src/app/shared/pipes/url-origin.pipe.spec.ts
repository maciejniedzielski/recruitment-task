import { UrlOriginPipe } from "./url-origin.pipe";

describe("UrlOriginPipe", () => {
    let pipe: UrlOriginPipe;

    beforeEach(() => {
        pipe = new UrlOriginPipe();
    });

    it("should create an instance", () => {
        expect(pipe).toBeTruthy();
    });

    it("should return url origin if url is given", () => {
        const result = pipe.transform("http://test.domain/test/1");
        expect(result).toBe("http://test.domain");
    });

    it("should return empty string if url not given", () => {
        const result = pipe.transform(null);
        expect(result).toBe("");
    });

    it("should return empty string if url is not valid", () => {
        const result = pipe.transform("test");
        expect(result).toBe("");
    });
});
