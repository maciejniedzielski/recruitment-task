import { ImagePlaceholderPipe } from "./image-placeholder.pipe";

describe("ImagePlaceholderPipe", () => {
    let pipe: ImagePlaceholderPipe;

    beforeEach(() => {
        pipe = new ImagePlaceholderPipe();
    });

    it("should create an instance", () => {
        expect(pipe).toBeTruthy();
    });

    it("should return url if url is given", () => {
        const result = pipe.transform("test");
        expect(result).toBe("test");
    });

    it("should return 'assets/images/placeholder.jpg' if url not given", () => {
        const result = pipe.transform(null);
        expect(result).toBe("assets/images/placeholder.jpg");
    });
});
