import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "urlOrigin",
})
export class UrlOriginPipe implements PipeTransform {
    transform(url: string): string {
        if (!url) {
            return "";
        }

        try {
            const parsedUrl: URL = new URL(url);

            return parsedUrl.origin;
        } catch (error) {
            return "";
        }
    }
}
