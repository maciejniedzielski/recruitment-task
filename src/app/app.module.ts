import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import {
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
} from "@angular/material";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { NewsApiKeyConfig, NgnewsModule } from "angular-news-api";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NewsDetailComponent } from "./news-detail/news-detail.component";
import { NewsComponent } from "./news/news.component";
import { NewsItemComponent } from "./news-item/news-item.component";
import { SharedModule } from "./shared/shared.module";
import { CommentComponent } from "./comment/comment.component";
import { environment } from "src/environments/environment";

const newsApiConfig: NewsApiKeyConfig = {
    key: environment.apiKey,
};

@NgModule({
    declarations: [
        AppComponent,
        NewsDetailComponent,
        NewsComponent,
        NewsItemComponent,
        CommentComponent,
    ],
    imports: [
        NgnewsModule.forRoot(newsApiConfig),
        SharedModule,
        BrowserModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        AppRoutingModule,
        MatToolbarModule,
        MatIconModule,
        MatMenuModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        MatProgressSpinnerModule,
        MatProgressBarModule,
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
